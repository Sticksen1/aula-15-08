package com.vc.imcaula1508;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toolbar;

public class IMC2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc2);
    }

    public void abreActivity(View v) {
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
